Scripts for building an image containing CPython and GraalVM
===================================================

Scripts in this repo are [Unlicensed ![](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/).

Third-party components have own licenses.

**DISCLAIMER: BADGES BELOW DO NOT REFLECT THE STATE OF THE DEPENDENCIES IN THE CONTAINER**

This builds a Docker image contains the following components:
  * `registry.gitlab.com/kolanich-subgroups/docker-images/fixed_python:latest` - a Docker image, each component of which has own license;
  * [`GraalVM`](https://github.com/oracle/graal) from [GitHub Releases ![](https://img.shields.io/github/downloads-pre/oracle/graal/latest/total.svg)](https://github.com/oracle/graal/releases) [![Licence](https://img.shields.io/github/license/oracle/graal.svg)](https://github.com/pypa/setuptools/blob/master/LICENSE)](https://github.com/oracle/graal/blob/master/LICENSE) [![Libraries.io Status](https://img.shields.io/librariesio/github/oracle/graal.svg)](https://libraries.io/github/oracle/graal) 
  * [`GraalPython`](https://github.com/graalvm/graalpython) [![Licence](https://img.shields.io/github/license/graalvm/graalpython.svg)](https://github.com/graalvm/graalpython/blob/master/LICENSE)](https://github.com/graalvm/graalpython/blob/master/LICENSE) [![Libraries.io Status](https://img.shields.io/librariesio/github/graalvm/graalpython.svg)](https://libraries.io/github/graalvm/graalpython) from `gu`
  * [`JPype1`](https://github.com/jpype-project/JPype) [![PyPi Status](https://img.shields.io/pypi/v/JPype1.svg)](https://pypi.python.org/pypi/JPype1) [![Licence](https://img.shields.io/github/license/jpype-project/JPype.svg)](https://github.com/jpype-project/jpype/blob/master/LICENSE) [![Libraries.io Status](https://img.shields.io/librariesio/github/jpype-project/JPype.svg)](https://libraries.io/github/jpype-project/JPype) from pypi 
  * dependencies of everytihing above.

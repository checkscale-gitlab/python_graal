#!/usr/bin/env sh
curl -o public.gpg https://kolanich-subgroups.gitlab.io/packages/GraalVM_deb/public.gpg
apt-key add ./public.gpg
rm ./public.gpg
echo deb [arch=amd64,signed-by=898bad1e937da3e70035b48a27805fb291a720d1] https://kolanich-subgroups.gitlab.io/packages/GraalVM_deb/repo/ stable contrib >> /etc/apt/sources.list.d/graal_KOLANICH.list
apt-get update
apt-get install --upgrade -y graalvm graalvm-gu graalvm-python graalvm-polyglot

export JAVA_HOME=/usr/lib/jvm/graalvm-ce-amd64
export PATH=$PATH:$JAVA_HOME/bin
java -version

